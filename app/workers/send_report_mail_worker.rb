class SendReportMailWorker
  include Sidekiq::Worker

  def perform(to, subject, report_filename, report_content)
    $logger.info('Sending mail to ' + to + ' with ' + report_filename + ' as attachment.')
    SendReportMailer.send_report(to, subject, report_filename, report_content).deliver_now
  end
end
