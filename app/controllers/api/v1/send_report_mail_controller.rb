module Api
  module V1
    class SendReportMailController < ApplicationController
      # caches_page :index, :show
      skip_before_action :verify_authenticity_token, :only => [:index, :send_report]

      def index
        render json: {status: 'SUCCESS', message:'MailSender API v1'},status: :ok
      end

      def send_report
        to = params[:to]
        subject = params[:subject]
        report_filename = params[:report_filename]
        report_content = params[:report_content]
        SendReportMailWorker.perform_async(to, subject, report_filename, report_content)
        render json: {status: 'SUCCESS', message:'Seu email está sendo enviado.'},status: :ok
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def send_report_mail_params
        params.require(:send_report_mail).permit(:to, :subject, :report_filename, :report_content)
      end
    end
  end
end
