class ApplicationMailer < ActionMailer::Base
  default from: "tiago.gondim@skyhub.com.br"
  layout 'mailer'
end
