class SendReportMailer < ApplicationMailer

  def send_report(to, subject, report_filename, report_content)
    @report_filename = report_filename
    @send_to = to
    attachments[report_filename] = report_content
    mail(to: to, subject: subject)
  end
end
