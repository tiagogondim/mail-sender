# mail-sender

##Desafio #6

Foi implementado um serviço apartado da aplicação para receber o relatório gerado pelo CadastroProdutos-desafio e enviá-lo por email.

# Atenção! 

Para executar este projeto, recomendam-se os seguintes requisistos:

- Ruby (2.2.9 or later);

- Rails (4.2.9 or later);

- Redis (4.0.1 or later, on localhost:6379);

- Sidekiq (5.1.3 or later);
