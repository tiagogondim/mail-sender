require "rails_helper"

RSpec.describe SendReportMailer, type: :mailer do
  # subject { described_class.new }

  before(:all) do
   @email_from = 'tiago.gondim@skyhub.com.br'
   @email_subject = 'Relatório do Cadastro de Produtos'
   @email_to = 'tiago.gondim@skyhub.com.br'
   @email_partial_body = "Email de exemplo enviado usando smtp."
   @email_attachment_name = 'RelatorioUserMailerTesteProdutos.csv'
   @email_content_type = "text/csv; filename=" + @email_attachment_name
   @report_content = 'sku,nome,descricao,quantidade,preco,ean\r\na,ad,a,22,22.0,1234567890123\r\nab,ab,abc,11,11.0,3543534543\r\nSKU1,Nome1,Descr1,10,111.0,3212345678901\r\nSKU2,Nome2,Descr2,222,2220.0,43543534522\r\nTesteBla,TesteBla,TesteBla,342,345.0,546546546546\r\njhgjhg,fghkhjk,jhkhj,7777,66.0,123456782222\r\nnovo,novo,novo,100,100.0,564654564\r\n'
  end

  describe 'com dados vállidos' do
   let(:mail) { SendReportMailer.send_report(@email_to, @email_subject, @email_attachment_name, @report_content) }

   it 'email enviado com sucesso' do
     expect(mail.subject).to eq(@email_subject)
     expect(mail.from).to have_content(@email_from)
     expect(mail.to).to have_content(@email_to)
     # expect(mail.body.encoded).to have_content(@email_partial_body)

     attachment = mail.attachments[0]
     expect(attachment).to match(Mail::Part)
     expect(attachment.content_type).to start_with(@email_content_type)
     expect(attachment.filename).to eq(@email_attachment_name)
   end
 end
end
