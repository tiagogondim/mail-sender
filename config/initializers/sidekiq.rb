host = RedisConfig.config["host"]
port = RedisConfig.config["port"]

db_num = '0'

Sidekiq.configure_server do |config|
  config.redis = { :url => "redis://#{host}:#{port}", expires_in: 90.minutes }
end

Sidekiq.configure_client do |config|
  config.redis = { :url => "redis://#{host}:#{port}", expires_in: 90.minutes }
end