require 'redis_config'

RedisConfig.config = YAML.load_file("config/redis.yml")[Rails.env]

