require 'logging'

begin
    Logging.appenders.stdout 'stdout',
                            :layout => Logging.layouts.pattern(:pattern => '[%d] %-5l: %m\n'),
                            :level => :debug
    Logging.appenders.rolling_file "file", \
                            :filename => "log/#{Rails.env}.log", :age => 'daily', :level => :debug,
                            :keep => 10, :layout => Logging.layouts.pattern(:pattern => '[%d] %-5l: {%M} %m\n')
    $logger = Logging.logger['MailSender']
    $logger.add_appenders "file"
    $logger.add_appenders "stdout" unless Rails.env.production?
    $logger
rescue Exception => e
    puts e
end
